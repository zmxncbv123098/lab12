CC = gcc
CFLAGS = 
LIBS = -lm

calc: calculate.o main.o
	  gcc calculate.o main.o  -o calc $(LIBS)

calculate.o: calculate.c calculate.h
			 gcc -c calculate.c $(CFLAGS)

main.o: main.c calculate.h
		gcc -c main.c $(CFLAGS)

clean:
		-rm calcul *.o *~
